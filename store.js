const fs = require('fs');

//Main
parse_command();
function parse_command(){	
	//This function Parses the command and redirects to the corresponding function

	var key = null;
	var value = null;
	var command = process.argv[2]?process.argv[2].toLowerCase():null;
	
	// console.log("Command:",command);
	switch (command){
		
		case 'add':
			key = process.argv[3]? process.argv[3] : null;
			value = process.argv[4]? process.argv[4] : null;
			if(key && value) 
				add(key,value);
			else
				console.log("=> Please, Enter a key and a value");
			break;
			
		case 'list':
			list();
			break;
			
		case 'get':
			key = process.argv[3]? process.argv[3] : null;
			if(key) 
				get(key);
			else
				console.log("=> Please, Enter a key");
			break;
			
		case 'remove':
			key = process.argv[3]? process.argv[3] : null;
			if(key) 
				remove(key);
			else
				console.log("=> Please, Enter a key");
			break;
			
		case 'clear':
			clear();
			break;
		default:
			console.log("=> Not a Valid Command");
			break;
	}
	
}


//Commands
function list(){
	read_list(function(dictionary){
		console.log("-------------------------------");
		console.log("=> Dictionary")
		console.log("-------------------------------");
		for(var i in dictionary){
			console.log(i + " -> " + dictionary[i])
		}
		console.log("-------------------------------");
	});
}
function get(key){
	read_list(function(dictionary){
		var value = dictionary[key]?dictionary[key]:null;
		if(value){	//checking if key exists
			console.log(value);
		}		
		else{
			console.log("=> This Key does not exist in the dictionary!");
		}
	});
}
function remove(key){
	read_list(function(dictionary){
		if(dictionary[key]){		//checking if key exists	
			delete dictionary[key]; 
			write_list(dictionary,function(){
				// console.log("=> Key was removed Successfully");
			});
		}
		else{
			console.log("=> This Key does not exist in the dictionary!");
		}
	});
}
function clear(){
	write_list({},function(){
		// console.log("=> List was cleared Successfully");
	});
}
function add(key,value){
	read_list(function(dictionary){
		dictionary[key] = value;
		write_list(dictionary,function(){
			// console.log("=> New Key/Value were saved Successfully");
		});
	});
}


//Reader and Writer
function read_list(cb){
	fs.readFile('./dictionary.json', 'utf8',(err, data) => {
		if (err) {
			console.log("Error Reading File");
			throw err;
		}
		else{
			var dictionary = JSON.parse(data);
			return cb(dictionary);
		}
	});
}
function write_list(dictionary,cb){
	fs.writeFile('./dictionary.json', JSON.stringify(dictionary,0,4), (err) => {
		if (err) {
			console.log("Error Writing File");
			throw err;
		}
		else{
			return cb();
		}
	});
}
